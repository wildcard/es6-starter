import * as _ from 'lodash'
import Greeter from './classes/greeter'
import './stylesheets/main.scss'

const greeter = new Greeter()

function output(content) {
  document.body.innerHTML = document.body.innerHTML + `<p>${content}</p>`
}

const names = ['Marek', 'Szymon', 'Tomek']

_.map(names, (name) => {
  output(greeter.greet(name))
})
