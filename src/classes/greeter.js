export default class Test {
  constructor(greeting = "Hello") {
    this.greeting = greeting
  }

  greet(name = "strager") {
    return `${this.greeting}, ${name}`
  }
}
